#pragma once

#include <SDL.h>
#include <vector>
#include <Texture.h>
#include "Math2D/Physics.h"
#include "Entities/Camera/Camera.h"

class RaycastRenderer
{
private:
	SDL_Renderer *Renderer;
	Physics *physicWorld;
	int screenW = 0, screenH = 0;
	int resolution = 320;
	SDL_Rect displayRects[320];

	void LoadTextures();
	void DisplayFlatWalls(int x, RaycastHit hit);
	void DisplayTexturedWalls(int x, RaycastHit hit, bool depthFog);


public:
	RaycastRenderer(SDL_Renderer *renderer, Physics* world);

	void Render(Camera *viewPoint);
	void RenderMinimap(Camera *viewPoint);
};

