#include "Texture.h"

Texture::Texture(std::string name, SDL_Texture *texture)
{
	id = name;
	tex = texture;
}

std::string Texture::GetId()
{
	return id;
}

SDL_Texture *Texture::GetTexture()
{
	return tex;
}
