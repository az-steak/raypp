#include <iostream>
#include <cmath>
#include <string>
#include <map>
#include <iterator>
#include "RaycastRenderer.h"

using namespace std;

const int TEXTURES_COUNT = 1;
static std::map<std::string, SDL_Texture *> textures {
	{"wall.bmp", nullptr}
};

RaycastRenderer::RaycastRenderer(SDL_Renderer *renderer, Physics* world) {
	Renderer = renderer;
	physicWorld = world;

	SDL_RendererInfo infos;
	SDL_GetRendererInfo(renderer, &infos);

	if (infos.flags & SDL_RENDERER_TARGETTEXTURE) {
		std::cout << "Can render texture" << std::endl;
		LoadTextures();
	}

	if (SDL_GetRendererOutputSize(Renderer, &screenW, &screenH) != 0)
		std::cerr << "Error getting renderer size" << std::endl;

	SDL_SetRenderDrawBlendMode(Renderer, SDL_BLENDMODE_BLEND);
}

void RaycastRenderer::LoadTextures() {
	for (std::map<std::string, SDL_Texture *>::iterator iterator = textures.begin(); iterator != textures.end(); ++iterator)
	{
		std::string path = "res/tex/" + iterator->first;
		SDL_Surface *surface = SDL_LoadBMP(path.data());
		iterator->second = SDL_CreateTextureFromSurface(Renderer, surface);
		SDL_FreeSurface(surface);
	}
}

void RaycastRenderer::DisplayFlatWalls(int x, RaycastHit hit)
{
	int rectWidth = screenW / resolution;
	int h = screenH / hit.distance;
	double f = (double)x / (double)resolution;
	displayRects[x].x = x * rectWidth;
	displayRects[x].y = screenH / 2 - h / 2;
	displayRects[x].w = rectWidth;
	displayRects[x].h = h;

	hit.distance = std::max(hit.distance / 2.0, 1.0);
	SDL_SetRenderDrawColor(Renderer, (255 * hit.texXRatio), (255 * (1 - hit.texXRatio)), 0, 255);
	SDL_RenderFillRect(Renderer, &displayRects[x]);
}

void RaycastRenderer::DisplayTexturedWalls(int x, RaycastHit hit, bool depthFog)
{
	SDL_Rect srcRect{ 0,0,0,0 };
	SDL_Rect displayRect{ 0, 0, 0,0 };
	int rectWidth = screenW / resolution;
	int h = screenH / hit.distance;

	SDL_QueryTexture(textures[hit.wall.GetTextureName() + ".bmp"], nullptr, nullptr, &srcRect.w, &srcRect.h);

	srcRect.x = srcRect.w * hit.texXRatio;
	srcRect.w = 1;
	displayRect.x = x * rectWidth;
	displayRect.y = screenH / 2 - h / 2;
	displayRect.w = rectWidth;
	displayRect.h = h;

	SDL_RenderCopy(Renderer, textures[hit.wall.GetTextureName() + ".bmp"], &srcRect, &displayRect);

	if (depthFog) {
		int fogIntensity = min(hit.distance * 10, 200.0);
		SDL_SetRenderDrawColor(Renderer, 0, 0, 0, fogIntensity);
		SDL_RenderFillRect(Renderer, &displayRect);
	}
}

void RaycastRenderer::Render(Camera *view) {

	SDL_SetRenderDrawColor(Renderer, 0, 0, 0, 255);
	SDL_RenderClear(Renderer);

	RaycastHit hit = RaycastHit();

	for (int x = 0; x < resolution; x++)
	{
		if (this->physicWorld->RaycastWall(view->position, view->GetForward((double)x / (double)resolution), hit)) {
			
			DisplayTexturedWalls(x, hit, true);
		}
	}
	SDL_RenderPresent(Renderer);
}

void RaycastRenderer::RenderMinimap(Camera *view) {
	int wallCount = this->physicWorld->walls.size();
	cout << wallCount << endl;
	int zoom = 10;

	SDL_SetRenderDrawColor(Renderer, 255, 0, 0, 255);

	for (int i = 0; i < wallCount; i++)
	{
		Wall wall = this->physicWorld->walls[i];

		int x1 = screenW / 2 + (wall.A.X - view->position.X) * zoom;
		int y1 = screenH / 2 + (wall.A.Y - view->position.Y) * zoom;
		int x2 = screenW / 2 + (wall.B.X - view->position.X) * zoom;
		int y2 = screenH / 2 + (wall.B.Y - view->position.Y) * zoom;

		SDL_RenderDrawLine(Renderer, x1, y1, x2, y2);
	}


	SDL_SetRenderDrawColor(Renderer, 255, 255, 0, 255);
	SDL_RenderDrawLine(Renderer, screenW / 2, screenH / 2, screenW / 2 + 20 * cos(view->orientation), screenH / 2 + 20 * sin(view->orientation));

	SDL_RenderPresent(Renderer);
}