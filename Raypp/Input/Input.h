#pragma once

#include <SDL.h>


class Input
{

protected:
	Input();

public:
	static Input* Instance;
	bool w = false, a = false, s = false, d = false, q = false, e = false, tab = false;

	static Input *GetInstance();
	void Update(const SDL_Event& events);

	double GetVertical();
	double GetRotation();
	double GetHorizontal();
};

