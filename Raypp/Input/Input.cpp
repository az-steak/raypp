#include <iostream>
#include "Input.h"


Input *Input::Instance = nullptr;

Input::Input()
{
}


Input *Input::GetInstance()
{
	if (Input::Instance == nullptr)
		Input::Instance = new Input();

	return Input::Instance;
}

void Input::Update(const SDL_Event &events)
{
	switch (events.type)
	{
	case SDL_KEYDOWN:
		if (events.key.keysym.scancode == SDL_SCANCODE_W)
			w = true;
		if (events.key.keysym.scancode == SDL_SCANCODE_S)
			s = true;

		if (events.key.keysym.scancode == SDL_SCANCODE_A)
			a = true;
		if (events.key.keysym.scancode == SDL_SCANCODE_D)
			d = true;

		if (events.key.keysym.scancode == SDL_SCANCODE_Q)
			q = true;
		if (events.key.keysym.scancode == SDL_SCANCODE_E)
			e = true;

		if (events.key.keysym.scancode == SDL_SCANCODE_TAB)
			tab = true;

		break;
	case SDL_KEYUP:
		if (events.key.keysym.scancode == SDL_SCANCODE_W)
			w = false;
		if (events.key.keysym.scancode == SDL_SCANCODE_S)
			s = false;

		if (events.key.keysym.scancode == SDL_SCANCODE_A)
			a = false;
		if (events.key.keysym.scancode == SDL_SCANCODE_D)
			d = false;

		if (events.key.keysym.scancode == SDL_SCANCODE_Q)
			q = false;
		if (events.key.keysym.scancode == SDL_SCANCODE_E)
			e = false;

		if (events.key.keysym.scancode == SDL_SCANCODE_TAB)
			tab = false;

		break;
	}
	
}

double Input::GetVertical()
{
	return (w ? 1.0 : 0.0) + (s ? -1.0 : 0.0);
}

double Input::GetRotation()
{
	return (a ? -1.0 : 0.0) + (d ? 1.0 : 0.0);
}

double Input::GetHorizontal()
{
	return (q ? -1.0 : 0.0) + (e ? 1.0 : 0.0);
}
