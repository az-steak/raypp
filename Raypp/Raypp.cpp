// Raypp.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <chrono>
#include <SDL.h>
#include "Time.h"
#include "RaycastRenderer.h"
#include <Input/Input.h>

using namespace std;

//Screen dimension constants
const int SCREEN_WIDTH = 1920;
const int SCREEN_HEIGHT = 1080;

bool run = true;

bool prevTab = false;
bool displayMap = false;

void initPhysics(Physics *&world) {
	world = new Physics();
	world->LoadMockData();
}

void updateInputs() {
	SDL_Event events;

	while (SDL_PollEvent(&events)) {
		switch (events.type)
		{
			case SDL_QUIT: 
				run = false;
				return;
				break;

			default :
				Input::Instance->Update(events);
				break;
		}
	}

}

void updateRender(RaycastRenderer* renderer, Camera* camera) {
	renderer->Render(camera);

	if (displayMap)
		renderer->RenderMinimap(camera);
}


int main(int argc, char *args[])
{
	SDL_Window *window(0);
	SDL_Renderer *renderer(0);
	RaycastRenderer *raycast(0);
	Physics* world(0);
	Camera *player(0);
	Input *input(0);

	if (SDL_CreateWindowAndRenderer(SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN, &window, &renderer) == -1)
		return -1;

	SDL_SetWindowTitle(window, "Raypp");
	initPhysics(world);

	chrono::steady_clock::time_point lastTime = chrono::high_resolution_clock::now();
	chrono::milliseconds ms(1000);
	Time::time = 0;
	Time::deltaTime = 0;
	

	input = Input::GetInstance();
	raycast = new RaycastRenderer(renderer, world);
	player = new Camera();

	while (run) {
		Time::deltaTime = (double)(chrono::high_resolution_clock::now() - lastTime).count() / 10000000;
		Time::time += Time::deltaTime;
		lastTime = chrono::high_resolution_clock::now();

		updateInputs();
		if (input->tab && !prevTab)
			displayMap = !displayMap;

		player->Update();

		updateRender(raycast, player);

		prevTab = input->tab;
	}

	delete(raycast);
	delete(world);
	delete(player);
	delete(input);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
