#pragma once

#include <string>

#include "../../Math2D/Vector2.h"

class Wall
{
private:
	double wallLength;
	std::string textureName;

public:
	Vector2 A, B;

	Wall();
	Wall(Vector2 a, Vector2 b);
	double GetLength();
	std::string GetTextureName();
};

