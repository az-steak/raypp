#include "Wall.h"

Wall::Wall() {
	Wall(Vector2(), Vector2());
}

Wall::Wall(Vector2 a, Vector2 b) {
	A = a;
	B = b;

	textureName = "wall";

	wallLength = Vector2::Distance(A, B);
}

double Wall::GetLength() {
	return wallLength;
}

std::string Wall::GetTextureName()
{
	return textureName;
}
