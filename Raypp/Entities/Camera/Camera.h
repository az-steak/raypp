#pragma once

#include "../../Math2D/Vector2.h"

class Camera
{
public:
	Vector2 position;
	double speed = 0.1;
	double rotationSpeed = .02;
	double orientation = 0;
	double fov = 70;


	Camera();
	Camera(double x, double y);

	void Update();
	Vector2 GetForward();
	Vector2 GetForward(double angleOffset);
	double GetRadFov();
};

