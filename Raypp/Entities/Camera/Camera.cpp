#include <cmath>
#include <iostream>
#include "Camera.h"
#include <Input/Input.h>

# define PI          3.141592653589793238462643383279502884L


double lerp(double a, double b, double t) {
	return a + t * (b - a);
}

Camera::Camera() {
	Camera(-5, 0);
}

Camera::Camera(double x, double y) {
	position = Vector2(x, y);
}

void Camera::Update()
{
	Vector2 forward = GetForward();
	Vector2 right = Vector2(-forward.Y, forward.X);
	position += forward * Input::Instance->GetVertical() * Time::deltaTime * speed;
	position += right * Input::Instance->GetHorizontal() * Time::deltaTime * speed;

	orientation += Input::Instance->GetRotation() * Time::deltaTime * rotationSpeed;
}

Vector2 Camera::GetForward()
{
	return GetForward(0.5);
}

Vector2 Camera::GetForward(double fovRatio) {
	double radFov = GetRadFov();
	double offset = lerp(-radFov / 2.0, radFov / 2.0, fovRatio);
	return Vector2(std::cos(orientation + offset), std::sin(orientation + offset));
}

double Camera::GetRadFov()
{
	return fov / 180 * PI;
}
