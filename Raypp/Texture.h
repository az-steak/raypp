#pragma once

#include <SDL.h>
#include <string>

class Texture
{
private:
	std::string id;
	SDL_Texture *tex;

public:
	Texture(std::string name, SDL_Texture *texture);
	std::string GetId();
	SDL_Texture *GetTexture();
};

