#pragma once
#include <vector>
#include "../Entities/Wall/Wall.h"
#include "Vector2.h"

struct RaycastHit {
	Wall wall;
	Vector2 point;
	double distance;
	double texXRatio;

	RaycastHit();
};

class Physics
{
private:

	bool RayIntersectSegment(Vector2 origin, Vector2 direction, Wall segment, RaycastHit *hit);

public:
	std::vector<Wall> walls;

	void LoadMockData();
	bool RaycastWall(Vector2 origin, Vector2 direction, RaycastHit &hit);
};

