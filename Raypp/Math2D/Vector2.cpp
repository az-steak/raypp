#include <cmath>
#include "Vector2.h"

Vector2::Vector2() {
	Vector2(0, 0);
}

Vector2::Vector2(double x, double y) {
	X = x;
	Y = y;
}

double Vector2::Length() {
	return sqrt(pow(X, 2) + pow(Y, 2));
}

void Vector2::Normalize() {
	double l = this->Length();
	
	this->X /= l;
	this->Y /= l;
}

Vector2 Vector2::Normalized() {
	double l = this->Length();

	return Vector2(this->X / l, this->Y / l);
}

double Vector2::Distance(Vector2 v1, Vector2 v2)
{
	return sqrt(pow(v1.X - v2.X, 2) + pow(v1.Y - v2.Y, 2));
}



Vector2& Vector2::operator+=(const Vector2 &v2) {
	this->X += v2.X;
	this->Y += v2.Y;
	return *this;
}

Vector2 Vector2::operator+(const Vector2 &v2) {
	return Vector2(this->X + v2.X, this->Y + v2.Y);
}

Vector2 Vector2::operator/(const double x) {
	return Vector2(this->X / x, this->Y / x);
}

bool Vector2::operator==(const Vector2 &v)
{
	return X == v.X && Y == v.Y;
}

Vector2 Vector2::operator*(const double x) {
	return Vector2(this->X * x, this->Y * x);
}

std::ostream &operator<<(std::ostream &os, const Vector2 &v)
{
	os << "(" << v.X << ", " << v.Y << ")";
	return os;
}
