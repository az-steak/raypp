#include <vector>
#include <iostream>
#include <cmath>
#include "Physics.h"

void Physics::LoadMockData() {
	walls = std::vector<Wall>(0);

	walls.push_back(Wall(Vector2(-20, -20), Vector2(20, -20)));
	walls.push_back(Wall(Vector2(20, -20), Vector2(20, 20)));
	walls.push_back(Wall(Vector2(20, 20), Vector2(-20, 20)));
	walls.push_back(Wall(Vector2(-20, 20), Vector2(-20, -20)));

	walls.push_back(Wall(Vector2(-20, 15), Vector2(12, -5)));
	walls.push_back(Wall(Vector2(-20, 10), Vector2(12, -10)));
}

bool Physics::RaycastWall(Vector2 origin, Vector2 direction, RaycastHit &hit) {
    bool hasHit = false;
    RaycastHit* nearest = nullptr;


	for (int i = 0; i < walls.size(); i++) {
        RaycastHit *lastHit = new RaycastHit();
        if (this->RayIntersectSegment(origin, direction, walls[i], lastHit)) {
            hasHit = true;

            if (nearest == nullptr || nearest->distance > lastHit->distance)
                nearest = lastHit;
            else
                delete(lastHit);
        }
        else
            delete(lastHit);
	}

    if (nearest != nullptr)
        hit = *nearest;
    

    return hasHit;
}

bool Physics::RayIntersectSegment(Vector2 origin, Vector2 direction, Wall segment, RaycastHit *hit) {
    double x1 = segment.A.X;
    double y1 = segment.A.Y;
    double x2 = segment.B.X;
    double y2 = segment.B.Y;

    double x3 = origin.X;
    double y3 = origin.Y;
    double x4 = x3 + direction.X;
    double y4 = y3 + direction.Y;

    double denominator = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
    
    if (denominator == 0)
        return false;

    double t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denominator;
    double u = -((x1 - x2) * (y1 - y3) - (y1 - y2) * (x1 - x3)) / denominator;

    if (0 < t && t < 1 && 0 < u) {
        double x = x1 + t * (x2 - x1);
        double y = y1 + t * (y2 - y1);

        hit->point = Vector2(x, y);
        hit->wall = segment;
        hit->distance = Vector2::Distance(hit->point, origin);
        hit->texXRatio = std::fmod(t * segment.GetLength(), 1);


        return true;
    }

    return false;
}

RaycastHit::RaycastHit()
{
    wall = Wall();
    point = Vector2();
    distance = 0;
    texXRatio = 0;
}
