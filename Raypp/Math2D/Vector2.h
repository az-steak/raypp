#pragma once
#include <iostream>

class Vector2
{
public :
	double X = 0, Y = 0;


	Vector2();
	Vector2(double x, double y);

	double Length();
	void Normalize();
	Vector2 Normalized();

	static double Distance(Vector2 v1, Vector2 v2);


	Vector2& operator+=(const Vector2 &v2);
	Vector2 operator+(const Vector2& v2);
	Vector2 operator*(const double x);
	Vector2 operator/(const double x);
	bool operator==(const Vector2 &v);
	friend std::ostream& operator<<(std::ostream &os, const Vector2& v);
};

